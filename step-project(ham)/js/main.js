// *************************************
// OUR SERVICES
// *************************************

let tabs_title = document.querySelectorAll('.our-services-item');
let tabs_desc = document.querySelectorAll('.our-services-desc');

for (let i = 0; i < tabs_title.length; i++) {
  tabs_title[i].dataset.index = i;
  tabs_desc[i].dataset.key = i;
  tabs_title[i].addEventListener('click', () => {
    tabs_title.forEach(x => {
      x.classList.remove('service-active');
    });
    tabs_desc.forEach(x => {
      x.style.display = 'none';
    });
    tabs_desc[i].style.display = 'flex';
    tabs_title[i].classList.add('service-active');
  });
}

tabs_desc.forEach(x => {
  if (x.dataset.key == 0) {
    return;
  }
  x.style.display = 'none';
});

// *************************************
// Our amazing works
// *************************************
let loadMore = $('.load-more');
let graphicDesign = $('.graphic-design');
let amazingImages = $('.our-work-img');
loadMore.on('click', addImages);
let n = 0;
function addImages() {
  n++;
  for (let i = 0; i < 24; i++) {
    amazingImages.eq(i).show();
  }
  if (n == 2) {
    amazingImages.show();
    loadMore.hide();
  }
}

let amazingOptionsLi = $('.our-work-li');
amazingOptionsLi.on('click', sortImages);
function sortImages() {
  amazingImages.hide();
  amazingOptionsLi.removeClass('our-work-active');
  $(this).addClass('our-work-active');
  loadMore.hide();
  n = 0;
  let dataKey = $(this).attr('dataKey');
  if (dataKey != undefined) {
    $(`.${dataKey}`).show();
  } else {
    graphicDesign.show();
    loadMore.css({ display: '' });
  }
}

// *************************************
// Our Slick
// *************************************

$('.slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
});
$('.slider-nav').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  asNavFor: '.slider-for',
  dots: true,
  centerMode: true,
  focusOnSelect: true
});
