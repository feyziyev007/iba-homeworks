var image = document.getElementById('img');
var currentImg = 0;
var images = ['img/a.png', 'img/b.png', 'img/c.png', 'img/d.png'];

let stopBtn = document.getElementById('stop_btn');
let resumeBtn = document.getElementById('resume_btn');

var myInterval = setInterval(volgendefoto, 2000);

stopBtn.addEventListener('click', function() {
  clearInterval(myInterval);
});

resumeBtn.addEventListener('click', function() {
  myInterval = setInterval(volgendefoto, 2000);
});

function volgendefoto() {
  if (++currentImg >= images.length) currentImg = 0;
  image.src = images[currentImg];
}
