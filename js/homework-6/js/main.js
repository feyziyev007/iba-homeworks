function filterBy(array, type) {
    let newArr = []

    array.forEach(element => {
        if (typeof (element) != type) {
            newArr.push(element)
        }
    });
    return newArr
}

let n = ['hello', 'world', 23, '23', null]
console.log(filterBy(n, 'string'));
