function displayArray(arr) {
    let createList = arr => {
        let ul = document.createElement('ul');
        if (Array.isArray(arr)) {
            arr.map(item => {
                let li = document.createElement('li');
                if (item === null) {
                    li.innerText = `null`
                }
                else if (typeof item === 'object') {
                    li.appendChild(createList(item))
                }
                else {
                    li.innerText = `${item}`
                }
                ul.appendChild(li)
            })
        }
        else {
            for (let i in arr) {
                let li = document.createElement("li");
                if (arr[i] === null) {
                    li.innerText = `${i} : null`;
                }
                else if (arr[i] === 'object') {
                    li.innerText = `${i} : `;
                    li.appendChild(create_List(arr[i]));
                }
                else {
                    li.innerText = `${i} : ${arr[i]}`;
                }
                ul.appendChild(li);
            }
        }
        return ul;
    }
    document.body.appendChild(createList(arr));
}

let a = ['hello', 'world', 'Baku', 'IBA Tech Academy', '2019', [2, 34, 222], { name: 'Azar', surname: 'Feyziyev', age: 20 }, null]
displayArray(a);
// let b = ['1', '2', '3', 'sea', 'user', 23]
// displayArray(b);

