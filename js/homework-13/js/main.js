let styles = document.querySelector('.styles');

if (localStorage.length > 0) {
  if (localStorage.getItem('style_state') == 'alternative') {
    styles.href = 'css/styleAlternative.css';
  }
} else {
  localStorage.setItem('style_state', 'default');
}

let change_button = document.querySelector('.change-button');

change_button.addEventListener('click', () => {
  if (localStorage.getItem('style_state') == 'alternative') {
    localStorage.setItem('style_state', 'default');
    styles.href = 'css/style.css';
  } else {
    localStorage.setItem('style_state', 'alternative');
    styles.href = 'css/styleAlternative.css';
  }
});
