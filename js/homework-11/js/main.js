let btn = document.querySelectorAll('.btn');
document.addEventListener('keydown', changeColor);

function changeColor(event) {
  btn.forEach(x => {
    x.classList.remove('blue');
  });
  for (let i = 0; i < btn.length; i++) {
    if (event.key == btn[i].textContent.toLowerCase()) {
      btn[i].classList.toggle('blue');
    } else if (event.key == 'Enter' && btn[i].textContent == 'Enter') {
      btn[i].classList.toggle('blue');
    }
  }
}
