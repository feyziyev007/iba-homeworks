let tabsTitle = $('.tabs-title');
let tabsText = $('.tabs-text');

for (let i = 0; i < tabsTitle.length; i++) {
  tabsTitle.eq(i).click(activeMaker);
  function activeMaker() {
    tabsTitle.removeClass('active');
    tabsTitle.eq(i).toggleClass('active');
    tabsText.hide();
    tabsText.eq(i).fadeIn(1000);
  }
}
